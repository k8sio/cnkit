package logger

import (
	"bytes"
	"fmt"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"
	"unicode"

	log "github.com/sirupsen/logrus"
)

type CustomFormatter struct {
	opt CustomOptions
}

func NewCustomFormatter(opts ...CustomOption) *CustomFormatter {
	l := &CustomFormatter{}
	for _, op := range opts {
		if op == nil {
			continue
		}
		op(&l.opt)
	}
	l.opt.withDefault()
	return l
}

type CustomOptions struct {
	format    string
	caller    bool
	goId      bool
	goIdWidth int
	utc       bool
}

type CustomOption func(options *CustomOptions)

func (l *CustomOptions) withDefault() {
	if l.format == "" {
		l.format = "2006-01-02T15:04:05.000Z0700"
	}
	if l.goIdWidth <= 0 {
		l.goIdWidth = 6
	}
}

func WithCustomTimeFormat(format string) CustomOption {
	return func(op *CustomOptions) {
		op.format = format
	}
}
func WithCustomCaller(enable bool) CustomOption {
	return func(op *CustomOptions) {
		op.caller = enable
	}
}

func WithCustomGoId(enable bool) CustomOption {
	return func(op *CustomOptions) {
		op.goId = enable
	}
}
func WithCustomGoIdWidth(width int) CustomOption {
	return func(op *CustomOptions) {
		op.goIdWidth = width
	}
}

func WithCustomUTC(enable bool) CustomOption {
	return func(op *CustomOptions) {
		op.utc = enable
	}
}

func (s *CustomFormatter) FormatOld(entry *log.Entry) ([]byte, error) {
	//timestamp := time.Now().Local().TimeFormat("0102-150405.000")
	timestamp := time.Now().Local().Format(s.opt.format)

	if s.opt.caller {
		var file string
		var line int
		if entry.Caller != nil {
			file = filepath.Base(entry.Caller.File)
			line = entry.Caller.Line
		}
		//fmt.Println(entry.Data)
		//msg := fmt.Sprintf("%s [%s:%d][GOID:%d][%s] %s\n", timestamp, file, line, getGID(), strings.ToUpper(entry.Level.String()), entry.Message)
		//msg := fmt.Sprintf("%s %s %s:%d [GOID:%d] %s\n", timestamp, strings.ToUpper(entry.Level.String()), file, line, getGID(), entry.Message)
		msg := fmt.Sprintf("%s %s %s:%d %s\n", timestamp, strings.ToUpper(entry.Level.String()), file, line, entry.Message)
		return []byte(msg), nil
	}

	msg := fmt.Sprintf("%s %s %s\n", timestamp, strings.ToUpper(entry.Level.String()), entry.Message)
	return []byte(msg), nil
}

func (s *CustomFormatter) Format(entry *log.Entry) ([]byte, error) {
	var timestamp string
	if s.opt.utc {
		timestamp = time.Now().UTC().Format(s.opt.format)
	} else {
		timestamp = time.Now().Local().Format(s.opt.format)
	}

	var fmtStr string
	var fmtLs []interface{}

	fmtStr += "%s "
	fmtLs = append(fmtLs, timestamp)

	if s.opt.goId {
		fmtStr += fmt.Sprintf("Go[%%0%dd] ", s.opt.goIdWidth)
		fmtLs = append(fmtLs, getGID())
	}

	fmtStr += "%s "
	//fmtLs = append(fmtLs, GetFirstCharacterToUpper(entry.Level.String()))
	fmtLs = append(fmtLs, strings.ToUpper(entry.Level.String()))

	if s.opt.caller {
		var file string
		var line int
		if entry.Caller != nil {
			file = filepath.Base(entry.Caller.File)
			line = entry.Caller.Line
		}
		fmtStr += "%s "
		fmtLs = append(fmtLs, fmt.Sprintf("%s:%d", file, line))
	}

	fmtStr += "%s"
	fmtLs = append(fmtLs, entry.Message)

	msg := fmt.Sprintf(fmtStr+"\n", fmtLs...)
	return []byte(msg), nil
}

func GetFirstCharacterToUpper(s string) string {
	if len(s) == 0 {
		return ""
	}
	runes := []rune(s)
	char := runes[0]
	return string(unicode.ToUpper(char))
}

func getGID() uint64 {
	b := make([]byte, 64)
	b = b[:runtime.Stack(b, false)]
	b = bytes.TrimPrefix(b, []byte("goroutine "))
	b = b[:bytes.IndexByte(b, ' ')]
	n, _ := strconv.ParseUint(string(b), 10, 64)
	return n
}
