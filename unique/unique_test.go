package unique

import (
	"github.com/google/uuid"
	"testing"
)

func Test_mgo_object_id(t *testing.T) {
	t.Log(GetObjectIdHex())
}

func Test_google_uuid(t *testing.T) {
	t.Log(uuid.Must(uuid.NewRandom()).String())
}

func Benchmark_mgo_object_id(b *testing.B) {
	b.StartTimer()
	for n := 0; n < b.N; n++ {
		_ = GetObjectIdHex()
	}
	b.StopTimer()
}

func Benchmark_google_uuid(b *testing.B) {
	b.StartTimer()
	for n := 0; n < b.N; n++ {
		_ = uuid.Must(uuid.NewRandom()).String()
	}
	b.StopTimer()
}
