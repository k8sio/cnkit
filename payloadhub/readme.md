# PayloadHub

PayloadHub 是一个高性能的处理数据生产/消费的工具包，特别适合于多个消费者并发处理相同生产数据的情况

Watcher callback 函数一般来说应该尽快返回避免阻塞，消费速率需要业务代码自行保证
> 如果业务逻辑较复杂或耗时较长，建议使用 chan 把数据发到其他地方专门处理  

如果 Watcher callback 耗时较长或发生了阻塞，可能会影响后续数据不能及时处理而被新 Push payload 覆盖掉
> 数据结构采用循环链表来存放 payload，当本轮链表节点耗尽后新 Push 的数据会循环覆盖之前较老的 payload