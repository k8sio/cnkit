package payloadhub

type PayloadHubInterface interface {
	Push(payload interface{}) bool
	Stop()
}

type PayloadWatcherInterface interface {
	Watcher(callback func(payload interface{}))
	Stop()
}
