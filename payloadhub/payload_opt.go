package payloadhub

type HubOptions struct {
	PayloadSize int
}

type HubOption func(opts *HubOptions)

func (opt *HubOptions) addOptions(options ...HubOption) {
	if opt.PayloadSize == 0 {
		opt.PayloadSize = 100000 // default
	}
	for _, option := range options {
		if option == nil {
			continue
		}
		option(opt)
	}
}

func (opt *HubOptions) withDefaults() (updated HubOptions) {
	updated = *opt
	if updated.PayloadSize < 2 {
		updated.PayloadSize = 2 // size 必须大于 1
	}
	return
}

func WithSize(size int) HubOption {
	return func(opt *HubOptions) {
		opt.PayloadSize = size
	}
}

type WatcherOptions struct {
	reliable bool
}

type WatcherOption func(opts *WatcherOptions)

func WithReliable(reliable bool) WatcherOption {
	return func(opts *WatcherOptions) {
		opts.reliable = reliable
	}
}
