package delayqueue

import (
	"log"
)

type Logger interface {
	Errorf(format string, args ...any)
}

type DefaultLogger struct {
}

func (*DefaultLogger) Errorf(format string, args ...any) {
	log.Default().Printf(format, args...)
}
