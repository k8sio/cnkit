package delayqueue

import (
	"strconv"
	"testing"
	"time"

	"github.com/alicebob/miniredis/v2"
	"github.com/go-redis/redis/v8"
)

func TestPublisher(t *testing.T) {
	minired, err := miniredis.Run()
	if err != nil {
		panic(err)
	}
	defer minired.Close()

	redisCli := redis.NewUniversalClient(&redis.UniversalOptions{
		Addrs: []string{minired.Addr()},
	})

	size := 1000
	retryCount := 3
	deliveryCount := make(map[string]int)
	cb := func(s string) bool {
		deliveryCount[s]++
		i, _ := strconv.ParseInt(s, 10, 64)
		return i%2 == 0
	}
	client := NewRedisV8Wrapper(redisCli)
	queue := NewQueue0("test", client, cb).WithLogger(&DefaultLogger{})
	publisher := NewPublisher0("test", client).WithLogger(&DefaultLogger{})

	for i := 0; i < size; i++ {
		err := publisher.SendDelayMsg(strconv.Itoa(i), 0, WithRetryCount(retryCount), WithMsgTTL(time.Hour))
		if err != nil {
			t.Error(err)
		}
	}
	for i := 0; i < 10*size; i++ {
		err := queue.consume()
		if err != nil {
			t.Errorf("consume error: %v", err)
			return
		}
	}
	for k, v := range deliveryCount {
		i, _ := strconv.ParseInt(k, 10, 64)
		if i%2 == 0 {
			if v != 1 {
				t.Errorf("expect 1 delivery, actual %d", v)
			}
		} else {
			if v != retryCount+1 {
				t.Errorf("expect %d delivery, actual %d", retryCount+1, v)
			}
		}
	}
}
